import { UsersStore } from './users';

export class RootStore {
  usersStore: UsersStore = new UsersStore();
}

const rootStore = new RootStore();

export default rootStore;
