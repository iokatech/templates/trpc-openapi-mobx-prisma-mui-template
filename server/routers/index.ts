import { t } from '../context';
import { userRouter } from './users';

export const appRouter = t.mergeRouters(userRouter);
