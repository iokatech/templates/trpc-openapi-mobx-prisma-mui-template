import { makeObservable, action, observable } from 'mobx';
import { User, CreateUser } from '../trpc';
import { CreateReactUtilsProxy } from '@trpc/react-query/shared';

import { AppRouter } from '../trpc';

export class UsersStore {
  users: User[] = observable([]);

  constructor() {
    makeObservable(this, {
      users: observable,
      addUser: action,
      deleteUser: action,
      listAllUsers: action,
      setUsers: action,
    });
  }

  setUsers = (users: User[]) => {
    this.users = [...users];
  };

  /**
   * Fetch all users.
   *
   * @param ctx this comes from trpc.useContext which is a hook so result must
   *            come from within the render of a functional component.
   */
  listAllUsers = async <TSSRContext>(
    ctx: CreateReactUtilsProxy<AppRouter, TSSRContext>
  ) => {
    const newUsers = await ctx.client.listUsers.query({});
    this.setUsers(newUsers);
  };

  /**
   * Add a new user.
   *
   * @param ctx this comes from trpc.useContext which is a hook so result must
   *            come from within the render of a functional component.
   */
  addUser = <TSSRContext>(
    ctx: CreateReactUtilsProxy<AppRouter, TSSRContext>,
    user: CreateUser
  ) => {
    ctx.client.addUser.mutate(user);
  };

  /**
   * Delete a user.
   *
   * @param ctx this comes from trpc.useContext which is a hook so result must
   *            come from within the render of a functional component.
   */
  deleteUser = <TSSRContext>(
    ctx: CreateReactUtilsProxy<AppRouter, TSSRContext>,
    user: User
  ) => {
    ctx.client.deleteUser.mutate({ id: user.id.toString() });
  };
}
