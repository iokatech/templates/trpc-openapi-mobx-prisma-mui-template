# A minimal React trpc-openapi-mobx-prisma-mui-template example

To start a new project based off of this template, first install `degit` which is
a project scaffolding tool:

```bash
yarn global add tiged # This is a new forked and maintained version of degit that also supports
                      # git subgroups
```

Use `degit` to clone the template:

```bash
degit --subgroup https://gitlab.com/iokatech/templates/trpc-openapi-mobx-prisma-mui-template trpc
```

## Prequisites

* Requires node 18 (for global fetch) and yarn.
* Install dependencies with:

```bash
yarn install # this also runs the prepare script which does the prisma generate and migrate
```

## Playing around

```bash
yarn dev
```

Open multiple browser windows to [The App](http://localhost:3000) and enjoy!

Try editing the ts files to see the type checking in action :)

## Linting

```bash
yarn lint
yarn lint-fix
```

## Building and Previewing Production

```bash
yarn build
yarn preview
```

## OpenAPI

In addition to using TRPC for client<->backend communication, there is also an
OpenAPI interface hosted on the backend.

To view the swagger UI for the OpenAPI interfaces, first run either
`yarn dev` or `yarn preview` and then you can visit:
[Swagger UI](http://localhost:2022/swagger).

## Developer Notes

To clear DB, production builds, and generated files:

```bash
yarn clean
```

To perform a DB migration - either after modifying the schema or initially:

```bash
yarn migrate
```

To re-generate ZOD schemas for Prisma models:

```bash
yarn generate
```

## E2E testing

To run E2E tests on the development environment:

```bash
yarn test-dev
```

To run E2E tests on the production build, you
first need to build the production environment with:

```bash
yarn build
```

Next, you can run tests with:

```bash
yarn test-preview
```
