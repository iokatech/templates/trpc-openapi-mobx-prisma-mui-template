import { test } from '@playwright/test';

// Set the overall test timeout to 35 seconds
test.setTimeout(35e3);

test('go to /', async ({ page }) => {
  page.setDefaultTimeout(5000);

  await page.goto('/');

  await page.waitForSelector(`text=Home Page`);
});
