import { createTRPCReact, createWSClient } from '@trpc/react-query';
import type { AppRouter } from '../../../server';
import { z } from 'zod';

/******************************************************************************
 * Export types used in the server.  This may involve using Zod infer to
 * convert Zod schema back to Typescript types.
 *
 * Add to the following as necessary for all shared types
 ******************************************************************************/

// Bring in prisma models, or other models that happen to come from the server
import { UserModel } from '../../../server/prisma/schemas';
import { WS_PORT } from '../../../server/env';

export { UserModel } from '../../../server/prisma/schemas';
export type User = z.infer<typeof UserModel>;
const CreateUserModel = UserModel.omit({ id: true });
export type CreateUser = z.infer<typeof CreateUserModel>;

/******************************************************************************
 * Export trpc interfaces - content below this probably doesn't need any
 * extra modifications
 ******************************************************************************/

export { SERVER_TRPC_URL, WS_PORT } from '../../../server/env';

// Make the AppRouter visible to the rest of the client
export type { AppRouter } from '../../../server';

// create persistent WebSocket connection
export const wsClient = createWSClient({
  url: `ws://localhost:${WS_PORT}`,
});

// Generate and export the main trpc handler
const trpc = createTRPCReact<AppRouter>();
export default trpc;
