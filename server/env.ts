export const TRPC_PATH = '/api/trpc';
export const REST_PATH = '/api/rest';

export const WS_PORT = 2023;
export const SERVER_PORT = 2022;
export const SERVER_BASE_URL = `http://localhost:${SERVER_PORT}`;
export const SERVER_TRPC_URL = `${SERVER_BASE_URL}${TRPC_PATH}`;
export const SERVER_REST_URL = `${SERVER_BASE_URL}${REST_PATH}`;
