import { inferAsyncReturnType, initTRPC } from '@trpc/server';
import { PrismaClient } from '@prisma/client';
import { OpenApiMeta } from 'trpc-openapi';
import trpcExpress from '@trpc/server/adapters/express';
import ws from 'ws';
import { IncomingMessage } from 'http';
import { NodeHTTPCreateContextFnOptions } from '@trpc/server/adapters/node-http';

const prismaClient = new PrismaClient();

/**
 * Creates context for an incoming request
 * @link https://trpc.io/docs/context
 */
export const createContext = async (
  opts:
    | trpcExpress.CreateExpressContextOptions
    | NodeHTTPCreateContextFnOptions<IncomingMessage, ws>
) => {
  return {
    req: opts.req,
    res: opts.res,
    prisma: prismaClient,
  };
};

export const t = initTRPC.meta<OpenApiMeta>().context<Context>().create();
export type Context = inferAsyncReturnType<typeof createContext>;
