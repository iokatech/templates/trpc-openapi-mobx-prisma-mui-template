import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { wsLink } from '@trpc/client';
import { useState } from 'react';
import { CssBaseline } from '@mui/material';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import trpc, { wsClient } from './trpc';

import HomePage from './pages/Home';

// Add to the following for all other 'pages"
const router = createBrowserRouter([
  {
    path: '/',
    element: <HomePage />,
  },
]);

export function App() {
  const [queryClient] = useState(() => new QueryClient());
  const [trpcClient] = useState(() =>
    trpc.createClient({
      links: [
        wsLink({
          client: wsClient,
        }),
      ],
    })
  );

  return (
    <trpc.Provider client={trpcClient} queryClient={queryClient}>
      <QueryClientProvider client={queryClient}>
        <CssBaseline />
        <RouterProvider router={router} />
      </QueryClientProvider>
    </trpc.Provider>
  );
}
