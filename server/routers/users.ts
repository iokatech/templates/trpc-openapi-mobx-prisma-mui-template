import { z } from 'zod';
import { t } from '../context';
import { PostsModel, UserModel } from '../prisma/schemas';
import { TRPCError } from '@trpc/server';
import { observable } from '@trpc/server/observable';
import EventEmitter from 'events';

const ee = new EventEmitter();

type User = z.infer<typeof UserModel>;
const UserCreateModel = UserModel.omit({ id: true });
const TAG = 'users';

export const userRouter = t.router({
  onAdd: t.procedure.subscription(() => {
    // `resolve()` is triggered for each client when they start subscribing `onAdd`
    // return an `observable` with a callback which is triggered immediately
    return observable<User>((emit) => {
      const onAdd = (data: User) => {
        // emit data to client
        emit.next(data);
      };
      // trigger `onAdd()` when `add` is triggered in our event emitter
      ee.on('add', onAdd);
      // unsubscribe function when client disconnects or stops subscribing
      return () => {
        ee.off('add', onAdd);
      };
    });
  }),

  onDelete: t.procedure.subscription(() => {
    // `resolve()` is triggered for each client when they start subscribing `onAdd`
    // return an `observable` with a callback which is triggered immediately
    return observable<string>((emit) => {
      const onDelete = (data: string) => {
        // emit data to client
        emit.next(data);
      };
      // trigger `onDelete()` when `delete` is triggered in our event emitter
      ee.on('delete', onDelete);
      // unsubscribe function when client disconnects or stops subscribing
      return () => {
        ee.off('delete', onDelete);
      };
    });
  }),

  addUser: t.procedure
    .input(UserCreateModel)
    .output(UserModel)
    .meta({
      /* 👉 */ openapi: {
        tags: [TAG],
        method: 'POST',
        path: `/${TAG}`,
        description: 'Add a new user',
      },
    })
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.create({ data: input });

      ee.emit('add', user);

      return user;
    }),
  getUser: t.procedure
    .input(
      z.object({
        id: z.string({ description: 'Unique ID of user to retrieve' }),
      })
    )
    .output(UserModel)
    .meta({
      /* 👉 */ openapi: {
        tags: [TAG],
        method: 'GET',
        path: `/${TAG}/{id}`,
        description: 'Get user',
      },
    })
    .query(({ input, ctx }) => {
      return new Promise<z.infer<typeof UserModel>>(async (resolve, reject) => {
        try {
          resolve(
            await ctx.prisma.user.findFirstOrThrow({
              where: { id: parseInt(input.id) },
            })
          );
        } catch (e) {
          reject(new TRPCError({ code: 'NOT_FOUND' }));
        }
      });
    }),
  listUsers: t.procedure
    .input(z.object({}))
    .output(
      z.array(
        z.intersection(
          UserModel,
          z.object({ posts: z.array(PostsModel.pick({ description: true })) })
        )
      )
    )
    .meta({
      /* 👉 */ openapi: {
        tags: [TAG],
        method: 'GET',
        path: `/${TAG}`,
        description: 'List all users',
      },
    })
    .query(({ ctx }) => {
      return ctx.prisma.user.findMany({
        include: { posts: { select: { description: true } } },
      });
    }),
  deleteUser: t.procedure
    .input(z.object({ id: z.string() }))
    .output(z.object({}))
    .meta({
      /* 👉 */ openapi: {
        tags: [TAG],
        method: 'DELETE',
        path: `/${TAG}/{id}`,
        description: 'Delete a user',
      },
    })
    .mutation(({ input, ctx }) => {
      return new Promise(async (resolve, reject) => {
        try {
          await ctx.prisma.user.delete({ where: { id: parseInt(input.id) } });

          ee.emit('delete', input.id);

          resolve({});
        } catch (e) {
          reject(new TRPCError({ code: 'NOT_FOUND' }));
        }
      });
    }),
});
