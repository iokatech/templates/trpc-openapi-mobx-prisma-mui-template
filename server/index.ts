import {
  generateOpenApiDocument,
  createOpenApiExpressMiddleware,
} from 'trpc-openapi';
import cors from 'cors';
import express from 'express';
import swaggerUi from 'swagger-ui-express';
import { createContext } from './context';
import { appRouter } from './routers';
import { applyWSSHandler } from '@trpc/server/adapters/ws';
import ws from 'ws';

import {
  REST_PATH,
  SERVER_REST_URL,
  SERVER_PORT,
  SERVER_BASE_URL,
  WS_PORT,
} from './env';

export type AppRouter = typeof appRouter;

const app = express();

// Setup CORS
app.use(cors());

// Handle incoming OpenAPI requests
app.use(
  REST_PATH,
  createOpenApiExpressMiddleware({ router: appRouter, createContext })
);

// Serve Swagger UI with our OpenAPI schema
app.use('/swagger', swaggerUi.serve);
app.get(
  '/swagger',
  swaggerUi.setup(
    generateOpenApiDocument(appRouter, {
      title: 'tRPC OpenAPI',
      version: '1.0.0',
      baseUrl: SERVER_REST_URL,
    })
  )
);

const wss = new ws.Server({
  port: WS_PORT,
});
applyWSSHandler({ wss, router: appRouter, createContext });
wss.on('connection', (ws) => {
  console.log(`Connection (${wss.clients.size})`);
  ws.once('close', () => {
    console.log(`Connection (${wss.clients.size})`);
  });
});
console.log(`WebSocket Server listening on ws://localhost:${WS_PORT}`);

app.listen(SERVER_PORT, () => {
  console.log(`Server started on ${SERVER_BASE_URL}`);
});
